<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use App\Http\Controllers\Controller;

class LastFMController extends Controller
{
    public function showTopArtistsByCountry($country = "Australia", $page = 1, $limit = 5)
    {
        $last_fm_api_key= env('LAST_FM_API_KEY', "824d5f76b5909140fa7eb85ee8c9ef42");
        $url = "http://ws.audioscrobbler.com/2.0/?method=geo.gettopartists&country=$country&api_key=$last_fm_api_key&limit=$limit&page=$page&format=json";

        $resource_payload = $this->getResourcesFromUrl($url, $page, $limit, 'topartists', 'artist');

        if(array_key_exists('error', $resource_payload)) {
            return view('last_fm.artists', ['error' => $resource_payload['error'], 'country' => $country, 'page' => 0, 'total_pages' => 0]);
        } else {
            return view('last_fm.artists', ['artists' => $resource_payload['objects'], 'country' => $country, 'page' => $page, 'total_pages' => $resource_payload['total_pages']]);
        }
    }

    public function showTopTracksForArtist($artist="Adele", $page = 1, $limit = 100)
    {
        $last_fm_api_key= env('LAST_FM_API_KEY', "824d5f76b5909140fa7eb85ee8c9ef42");
        $url = "http://ws.audioscrobbler.com/2.0/?method=artist.gettoptracks&artist=$artist&api_key=$last_fm_api_key&limit=$limit&page=$page&format=json";

        $resource_payload = $this->getResourcesFromUrl($url, $page, $limit, 'toptracks', 'track');

        if(array_key_exists('error', $resource_payload)) {
            return view('last_fm.songs', ['error' => $resource_payload['error'], 'artist' => $artist]);
        } else {
            return view('last_fm.songs', ['songs' => $resource_payload['objects'], 'artist' => $artist]);
        }
    }

    private function getResourcesFromUrl($url, $page, $limit, $object_container_name, $object_name)
    {
        $client = new Client();
        $response = $client->request('GET', $url);

        $responsePayload = json_decode($response->getBody());
    
        if(isset($responsePayload->error)) {
            return ['error' => $responsePayload->message];
        } else {
            $attr_property = '@attr';
            $total_pages = $responsePayload->$object_container_name->$attr_property->totalPages;

            $objects = $responsePayload->$object_container_name->$object_name;
            $objects_count = sizeof($objects);
            if( $objects_count > $limit) {
                $objects = array_slice($objects, $objects_count - $limit, $limit );
            }

            return ['objects' => $objects, 'page' => $page, 'total_pages' => $total_pages];
        }
    }
}
