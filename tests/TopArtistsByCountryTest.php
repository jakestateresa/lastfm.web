<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TopArtistsByCountryTest extends TestCase
{
    public function testInvalidCountryReturnsAnError()
    {
        $response = $this->action('GET', 'LastFMController@showTopArtistsByCountry', ['country' => 'omgwtfbbq']);
        $this->assertViewHas('error', 'country param invalid');
        $this->assertResponseOk();
    }

    public function testViewGetsValidValuesOnASuccessfulRequest()
    {
        $response = $this->action('GET', 'LastFMController@showTopArtistsByCountry', ['country' => 'China']);

        $this->assertViewMissing('error');
        $this->assertViewHas('country', 'China');
        $this->assertViewHas('page', 1);
        $this->assertViewHas('artists');
        $this->assertViewHas('total_pages');
        $this->assertResponseOk();
    }
}
