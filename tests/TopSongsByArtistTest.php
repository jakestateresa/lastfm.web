<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TopSongsByArtistTest extends TestCase
{
    public function testInvalidArtistReturnsAnError()
    {
        $response = $this->action('GET', 'LastFMController@showTopTracksForArtist', ['artist' => 'oqoqoqoqoo']);

        $this->assertViewMissing('songs');

        $this->assertViewHas('error', 'The artist you supplied could not be found');

        $this->assertResponseOk();
    }

    public function testViewGetsValidValuesOnASuccessfulRequest()
    {
        $response = $this->action('GET', 'LastFMController@showTopTracksForArtist', ['artist' => 'Soundgarden']);

        $this->assertViewMissing('error');

        $this->assertViewHas('artist', 'Soundgarden');
        $this->assertViewHas('songs');
        
        $this->assertResponseOk();
    }
}
