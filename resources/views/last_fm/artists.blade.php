@extends('layouts.master')

@section('title', 'Top Artists for {{ $country }}')

@section('content')
    <h4>Top Artists for {{ $country }}</h5>
    <?php
    $base_url = action('LastFMController@showTopArtistsByCountry');
    ?>
    <form action="get" id="search_form" onSubmit="javascript:window.location='{{ $base_url }}/' + $('#country').val();return false;">
    <div class="input-group">
        <input type="text" class="form-control" placeholder="Country" aria-describedby="basic-addon2" id="country"<?php if(isset($country)) { ?>value="{{ $country }}"<?php } ?> />
        <span class="input-group-btn">
            <button class="btn btn-default" type="button">Go!</button>
        </span>
    </div>
    </form>

    <?php
    if(isset($error)){
    ?>
    <br/>
    <div class="alert alert-danger" role="alert">{{ $error }}</div>
    <?php
    }
    ?>
    <?php
    $previous_disabled = false;
    $previous_page_url = '#';
    if($page > 0) {
        $previous_page_url = action('LastFMController@showTopArtistsByCountry', ['country' => $country, 'page' => $page - 1]);
        if($page == 1){
            $previous_disabled = true;
            $previous_page_url = '#';
        }
    }

    $next_page_disabled = false;
    $next_page_url = '#';
    if($total_pages > 0) {
        $next_page_url = action('LastFMController@showTopArtistsByCountry', ['country' => $country, 'page' => $page + 1]);
        if($page == $total_pages) {
            $next_page_disabled = true;
            $next_page_url = '#';
        }
    }
    ?>
    <nav>
        <ul class="pager">
            <li class="previous<?php if($previous_disabled) { ?> disabled<?php }?>"><a href="{{ $previous_page_url }}"><span aria-hidden="true">&larr;</span> Previous</a></li>
            <li class="next<?php if($next_page_disabled) { ?> disabled<?php }?>"><a href="{{ $next_page_url }}">Next <span aria-hidden="true">&rarr;</span></a></li>
        </ul>
    </nav>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Thumbnail</th>
                <th>Name</th>
                <th>Links</th>
            </tr>
        </thead>
        <?php
        if(isset($artists))
        {
        ?>
        @foreach ($artists as $artist)
            <?php
                $top_tracks_url = action('LastFMController@showTopTracksForArtist', ['artist' => $artist->name]);
                $image_url_property =  "#text";
                $thumbnail = array_filter(
                    $artist->image,
                    function ($image) {
                        return $image->size == 'small';
                    }
                )[0];
            ?>
            <tr>
                <td><img src="{{ $thumbnail->$image_url_property }}" /></td>
                <td> {{ $artist->name }}</td>
                <td><a href="{{ $top_tracks_url }}">Show Top Tracks</a></td>
            </tr>
        @endforeach
        <?php
        }
        ?>
    </table>
    <nav>
        <ul class="pager">
            <li class="previous<?php if($previous_disabled) { ?> disabled<?php }?>"><a href="{{ $previous_page_url }}"><span aria-hidden="true">&larr;</span> Previous</a></li>
            <li class="next<?php if($next_page_disabled) { ?> disabled<?php }?>"><a href="{{ $next_page_url }}">Next <span aria-hidden="true">&rarr;</span></a></li>
        </ul>
    </nav>

    </div> <!-- /container -->
@stop
