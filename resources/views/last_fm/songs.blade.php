@extends('layouts.master')

@section('title', 'Top Tracks for {{ $artist }}')

@section('content')
    <h4>Top Tracks for {{ $artist }}</h5>
    <?php
    if(isset($error)){
    ?>
    <div class="alert alert-danger" role="alert">{{ $error }}</div>
    <br/>
    <?php
    }
    ?>
    <nav>
        <ul class="pager">
            <li class="previous"><a href="javascript:history.back()"><span aria-hidden="true">&larr;</span> Back</a></li>
        </ul>
    </nav>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Thumbnail</th>
                <th>Title</th>
            </tr>
        </thead>
        <?php
        if(isset($songs))
        {
        ?>
        @foreach ($songs as $song)
            <?php
                $image_url_property =  "#text";
                $thumbnail = array_filter(
                    $song->image,
                    function ($image) {
                        return $image->size == 'small';
                    }
                )[0];
            ?>
            <tr>
                <td><img src="{{ $thumbnail->$image_url_property }}" /></td>
                <td> {{ $song->name }}</td>
            </tr>
        @endforeach
        <?php
        }
        ?>
    </table>
    <nav>
        <ul class="pager">
            <li class="previous"><a href="javascript:history.back()"><span aria-hidden="true">&larr;</span> Back</a></li>
        </ul>
    </nav>

    </div> <!-- /container -->
@stop
