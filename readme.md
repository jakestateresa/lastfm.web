# Overview #

This repository contains the source code for a php website created using the Laravel Framework. It makes use of the LastFM api to show the top artists for a given country as well as the top hits for an artist. 

![LastFM Top Artists By Country](/LastFM.Web.png)

### Features ###

 * Allow the user to enter a country on the search bar
 * Shows the name and image of top artists from the given country
 * Pagination via Previous and Next buttons 
 * Shows the top hits by the selected artist

### Environment Setup ###

 * Install PHP 5.5.9 or higher
 * Install Composer
 * Download the source code of the application from [downloads page](https://bitbucket.org/jakestateresa/lastfm.web/downloads) or clone the repository using the command ```git clone https://bitbucket.org/jakestateresa/lastfm.web.git```
 * Extract the source code in a suitable directory

### Running using PHP's Builtin Server ###

 * Open the command-line 
 * Change the directory to the folder containing the lastfm.web artifacts
 * Type the command ```php artisan serve```
 * Open the following url in a browser ```http://localhost:8000/artist/list/```

### Running Unit Tests ###

 * Open the command-line 
 * Change the directory to the folder containing the lastfm.web artifacts
 * Type the command ```./vendor/bin/phpunit```